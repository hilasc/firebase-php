<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angular-d3b02.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/invoices';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	switch ($_POST['submit']) {
		case 'Create':
			if (isset($_POST["product"]) && !$_POST["product"] == '' && isset($_POST["amount"]) && !$_POST["amount"] == '') { //Create invoice
				$invoice = array(
				"product" => $_POST["product"],
				"amount" => $_POST["amount"]
				);
				
				$firebase->push(DEFAULT_PATH.'', $invoice);
			}
		break;
	}
}


?>

<html>

	<head><title>Invoices</title></head>
	<body>
		<script>
		function displayCreate() {
			document.getElementById("form").innerHTML = document.getElementById("create_form").innerHTML;
		}
		function displayUpdate() {
			document.getElementById("form").innerHTML = document.getElementById("update_form").innerHTML;
		}
		function displayDelete() {
			document.getElementById("form").innerHTML = document.getElementById("delete_form").innerHTML;
		}
		</script>
		<center><h1>Invoices</h1>
	
		<?php 
		$all_invoices = json_decode($firebase->get(DEFAULT_PATH), true); //convert result string to array
															// if false - as JSON object
		?>
		
		<table border=1>
			<th>Id</th>
			<th>Product Name</th>
			<th>Amount</th>
		<?php 
		foreach ($all_invoices as $id=>$invoice) { // all_invoices is array. every element in array is map (id=>map)
												// $id is the key, $invoice is the value (name,email,posts)
		?>
			<tr>
				<td><?php  echo $id	?></td>
				<td><?php  echo $invoice['product']	?></td>
				<td><?php  echo $invoice['amount']	?></td>
			</tr>
				
		<?php }	?>
		</table>

		<div><br>
			<form name="actions">
				<input type="button" name="create" value="Add Invoice" onclick="displayCreate()">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
			</form>
		</div>
		
		<div id="form">
		
		</div>
		
		<!-- div for create form -->
		<div id="create_form" style="display:none;">
			<h2>Create invoice</h2>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				Product Name:  <input type="text" name="product"></br>
				Amount: <input type="text" name="amount"></br></br>
				
				<input type="submit" name="submit" value="Create" >
				<input type="reset" value="Reset">
			</form>
		</div>
	</body>
</html>