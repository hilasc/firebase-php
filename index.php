<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angular-d3b02.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

// --- storing an array ---

//update existing user1 with "update"
$user1 = array(
	"name" => "test from php7",
);
$firebase->update(DEFAULT_PATH.'/2/', $user1);

//replace existing user 2 with "set"
$user2 = array(
	"name" => "test from php33",
);
$firebase->set(DEFAULT_PATH.'/3/', $user2);

//create newUser with "push"
$newUser = array(
	"name" => "new User",
	"email" => "newUser@test.com",
	"posts" => [new1=>true, new2=>true]
);
$newUserId = $firebase->push(DEFAULT_PATH.'', $newUser);
echo $newUserId;
echo '<br><br>';

//-- delete user 4 with delete
$firebase->delete(DEFAULT_PATH.'/4/');

//--get User1 with get
$user1 = $firebase->get(DEFAULT_PATH.'/2/');

echo $user1;

echo '<p>All OK</p>';