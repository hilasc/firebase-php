<?php
require 'src/firebaseLib.php';
const DEFAULT_URL = 'https://angular-d3b02.firebaseio.com/';
const DEFAULT_TOKEN = '';
const DEFAULT_PATH = '/users';

$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	switch ($_POST['submit']) {
		case 'Create':
			if (isset($_POST["name"]) && !$_POST["name"] == '' && isset($_POST["email"]) && !$_POST["email"] == '') { //Create user
				$user = array(
				"name" => $_POST["name"],
				"email" => $_POST["email"]
				);
				
				$firebase->push(DEFAULT_PATH.'', $user);
			}
		break;
		
		case 'Update':
			if (!$_POST["u_id"] == '') { //Update user
					$u_user = array(
					"name" => $_POST["u_name"],
					"email" => $_POST["u_email"]
					);
					$firebase->update(DEFAULT_PATH.'/'.$_POST["u_id"], $u_user);
			}
		break;
		
		case 'Delete':
			if (!$_POST["u_id"] == '') { //Delete user
				$firebase->delete(DEFAULT_PATH.'/'.$_POST["u_id"]);
			}
		break;
	}
}


?>

<html>

	<head><title>Usres</title></head>
	<body>
		<script>
		function displayCreate() {
			document.getElementById("form").innerHTML = document.getElementById("create_form").innerHTML;
		}
		function displayUpdate() {
			document.getElementById("form").innerHTML = document.getElementById("update_form").innerHTML;
		}
		function displayDelete() {
			document.getElementById("form").innerHTML = document.getElementById("delete_form").innerHTML;
		}
		</script>
		<center><h1>Users</h1>
	
		<?php 
		$all_users = json_decode($firebase->get(DEFAULT_PATH), true); //convert result string to array
															// if false - as JSON object
		?>
		
		<table border=1>
			<th>Id</th>
			<th>Name</th>
			<th>Email</th>
		<?php 
		foreach ($all_users as $id=>$user) { // all_users is array. every element in array is map (id=>map)
												// $id is the key, $user is the value (name,email,posts)
		?>
			<tr>
				<td><?php  echo $id	?></td>
				<td><?php  echo $user['name']	?></td>
				<td><?php  echo $user['email']	?></td>
			</tr>
				
		<?php }	?>
		</table>

		<div><br>
			<form name="actions">
				<input type="button" name="create" value="Add User" onclick="displayCreate()">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<input type="button" name="update" value="Update User" onclick="displayUpdate()">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
				<input type="button" name="delete" value="Delete User" onclick="displayDelete()">
			</form>
		</div>
		
		<div id="form">
		
		</div>
		
		<!-- div for create form -->
		<div id="create_form" style="display:none;">
			<h2>Create user</h2>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				Name:  <input type="text" name="name"></br>
				Email: <input type="text" name="email"></br></br>
				
				<input type="submit" name="submit" value="Create" >
				<input type="reset" value="Reset">
			</form>
		</div>
		
		<!-- div for update form -->
		<div id="update_form" style="display:none;">
			<h2>Update user</h2>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				ID: <input type="text" name="u_id">
				Name:  <input type="text" name="u_name"></br>
				Email: <input type="text" name="u_email"></br></br>
				
				<input type="submit" name="submit" value="Update">
				<input type="reset" value="Reset">
			</form>
		</div>
		
		<!-- div for delete form -->
		<div id="delete_form" style="display:none;">
			<h2>Update user</h2>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				ID: <input type="text" name="u_id">
				
				<input type="submit" name="submit" value="Delete">
			</form>
		</div>
	</body>
</html>